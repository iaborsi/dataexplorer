# -*- coding: utf-8 -*-

#******************************************************************************
#
# Data Explorer
# ---------------------------------------------------------
# Provides tools for statistical data exploration.
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

import os
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtGui, uic

from qgis.core import *

#from ui.ui_dataexplorer import Ui_DataExplorerDialog

import dataexplorer_utils as utils
import statutils as sts
from scipy import stats as scystat

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
if matplotlib.__version__ >= '1.5.0':
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
else:
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar

from matplotlib.figure import Figure
from matplotlib import rcParams
import pylab as pl
import numpy as np
from numpy import arange

#
import datetime as dt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
#

class DataExpDialog(QDialog):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        uic.loadUi(os.path.join( os.path.dirname(__file__), 'ui/ui_dataexplorer.ui'), self)

        #self.setupUi(self)

        # add matplotlib figure to dialog
        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self.figure)
        self.mpltoolbar = NavigationToolbar(self.canvas, self.widgetPlot)
        lstActions = self.mpltoolbar.actions()
        self.mpltoolbar.removeAction(lstActions[7])
        self.layoutPlot.addWidget(self.canvas)
        self.layoutPlot.addWidget(self.mpltoolbar)

        # and configure matplotlib params
        rcParams["font.serif"] = "Verdana, Arial, Liberation Serif"
        rcParams["font.sans-serif"] = "Tahoma, Arial, Liberation Sans"
        rcParams["font.cursive"] = "Courier New, Arial, Liberation Sans"
        rcParams["font.fantasy"] = "Comic Sans MS, Arial, Liberation Sans"
        rcParams["font.monospace"] = "Courier New, Liberation Mono"

        self.tab_2.setEnabled(True)

        self.cmbLayers.currentIndexChanged.connect(self.reloadFields)
        self.cmbLayers_2.currentIndexChanged.connect(self.reloadFields_2)

        # timeformatList = ['%Y-%m-%d', 'yyyy-MM-dd','%Y/%m/%d', '%d/%m/%Y','%d-%m-%Y', '%m-%d-%Y', '%m/%d/%Y', '%Y-%m-%d %H:%M:%S', '%H:%M:%S']
        # self.cmbTimeFormat.addItems(timeformatList)


        self.chkShowGrid.stateChanged.connect(self.addGrid)
        self.btnBrowse.clicked.connect(self.outFilecsv)

        self.manageGui()
        self.axes.set_title(self.tr(" "))
        #
        self.buttonShow.clicked.connect(self.dataPlot)
        #
        self.btnSteady.clicked.connect(self.dataSteadyPlot)
        #
        self.buttonShow_2.clicked.connect(self.multiPlot)
        self.buttonShow_3.clicked.connect(self.scatterPlot)

        self.buttonQQplot.clicked.connect(self.qqPlot)
        self.buttonKStest.clicked.connect(self.ksTest)

        self.buttonPeriodogram.clicked.connect(self.periodogramPlot)
        self.buttonAcf.clicked.connect(self.acfPlot)
        self.buttonTrend.clicked.connect(self.trendPlot)

    def manageGui(self):
        self.cmbLayers.clear()
        self.cmbLayers.addItems(utils.getVectorLayerNames())
        self.cmbLayers_2.clear()
        self.cmbLayers_2.addItems(utils.getVectorLayerNames())
        self.buttonShow.setEnabled(True)
        self.buttonShow_2.setEnabled(True)
        self.buttonShow_3.setEnabled(True)
        self.buttonTrend.setEnabled(True)
        #~ if self.chkTimeFieldSlct.isChecked():
			#~ timeformatList = ['d/m/y','d-m-y', 'm-d-y', 'm/d/y', 'y-m-d H:M:S']
			#~ self.cmbTimeFormat.addItems(timeformatList)
		#else:
		#	self.cmbTimeFormat.clear()

    #
    def fileDialog(parent):
        myfile = QFileDialog.getSaveFileName(parent, "Choose a file", "", "*.csv")
        return unicode(myfile)
    #
    def outFilecsv(self):
        self.OutFilePath = self.fileDialog()
        self.txtCsv.setText(self.OutFilePath)

    def reloadFields(self):
        self.cmbFields.clear()
        self.cmbTimeStamp.clear()
        self.cmbDate.clear()
        self.cmbTime.clear()


        self.axes.clear()

        layer = utils.getVectorLayerByName(self.cmbLayers.currentText())
        # Check if selection exists:
        if layer.selectedFeatureCount() != 0:
            self.chkUseSelected.setCheckState(Qt.Checked)
        else:
            self.chkUseSelected.setCheckState(Qt.Unchecked)

        # Conversely: if check box is selected and NO selection exists ... Warning!!
        if self.chkUseSelected.isChecked() and \
                layer.selectedFeatureCount() == 0:
            QMessageBox.warning(self,
                                self.tr('No selection'),
                                self.tr('There is no selection in input '
                                        'layer. Uncheck corresponding option '
                                        'or select some features before '
                                        'running analysis'))
            return

        if self.tab_2.isVisible():
            self.cmbFields_2.clear()
            layer2 = utils.getVectorLayerByName(self.cmbLayers_2.currentText())
            self.cmbFields_2.addItems(utils.getFieldNames(layer2, [QVariant.Int, QVariant.Double]))

            self.cmbTime.addItems(utils.getFieldNames(layer, [QVariant.Int, QVariant.Double, QVariant.String]))

        self.cmbFields.addItems(utils.getFieldNames(layer, [QVariant.String, QVariant.Int, QVariant.Double]))
        self.cmbTimeStamp.addItems(utils.getFieldNames(layer, [QVariant.DateTime, QVariant.Date, QVariant.Time,  QVariant.Int, QVariant.Double, QVariant.String]))
        self.cmbDate.addItems(utils.getFieldNames(layer, [QVariant.DateTime, QVariant.Date, QVariant.Time,  QVariant.Int, QVariant.Double, QVariant.String]))
        self.cmbTime.addItems(utils.getFieldNames(layer, [QVariant.DateTime, QVariant.Date, QVariant.Time,  QVariant.Int, QVariant.Double, QVariant.String]))

    def reloadFields_2(self):

        self.axes.clear()

        self.cmbFields_2.clear()
        layer2 = utils.getVectorLayerByName(self.cmbLayers_2.currentText())
        self.cmbFields_2.addItems(utils.getFieldNames(layer2, [QVariant.Int, QVariant.Double]))


    def reject(self):
        QDialog.reject(self)

    def setProgressRange(self, maxValue):
        self.progressBar.setRange(0, maxValue)

    def updateProgress(self):
        self.progressBar.setValue(self.progressBar.value() + 1)

    def processFinished(self, statData):
        self.stopProcessing()

    def processInterrupted(self):
        self.restoreGui()

    def stopProcessing(self):
        if self.workThread is not None:
            self.workThread.stop()
            self.workThread = None

    def restoreGui(self):
        self.progressBar.setFormat("%p%")
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(0)

        #self.buttonBox.rejected.connect(self.reject)
        #self.btnClose.clicked.disconnect(self.stopProcessing)
        #self.btnClose.setText(self.tr("Close"))
        self.btnOk.setEnabled(True)

    def addGrid(self):
        self.axes.grid(self.chkShowGrid.isChecked())

    def dataPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()

        #
        # check for (possible) selection on the selected layer
        useSelection = self.chkUseSelected.isChecked()

        # check for (possible) selection of time field

        useTimeField = self.chkTimeFieldSlct.isChecked()
        #
        if useTimeField:
            if self.radioTimeStamp.isChecked():
                timestampField = self.cmbTimeStamp.currentText()
                timestampFormat = self.cmbTimeStampFormat.currentText()
            else:
                dateField = self.cmbDate.currentText()
                dateFormat = self.cmbDateFormat.currentText()
                timeField = self.cmbTime.currentText()
                timeFormat = self.cmbTimeFormat.currentText()


        index = layer.fieldNameIndex(fieldName)
        #
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        #
        #
        if useTimeField:
            if self.radioDateTime.isChecked():
                dateIndex = layer.fieldNameIndex(dateField)
                # requestDate = QgsFeatureRequest()
                # requestDate.setFlags(QgsFeatureRequest.NoGeometry)
                # requestDate.setSubsetOfAttributes([timeIndex])
            tempo = []
        #
        sdat = []
        #


        #
        if useSelection:
            selection = layer.selectedFeatures()
            count = layer.selectedFeatureCount()
            for f in selection :
                if f[index]:
                    stemp = float(f[index])
                else:
                    stemp = 0.0

                sdat.append(stemp)

			#
            if useTimeField :
                # Case in which Unique TimeStamp is used
                if self.radioTimeStamp.isChecked():
                    for ftime in selection :
                        if type(ftime[timestampField]) == QDateTime :
                            timeTostring = ftime[timestampField].toString('yyyy-MM-dd H:M:S')
                            ttemp = ftime[timestampField].toPyDateTime()
                            # ttemp = dt.datetime.strptime(timeTostring,timestampFormat)
                        else:
                            ttemp = dt.datetime.strptime(ftime[timestampField],timestampFormat)
                        tempo.append(ttemp)
                        # if ftime[timeIndex]:
                        #     ttemp = dt.datetime.strptime(ftime[timeIndex],timeFormat)
                        # else:
                        #     ttemp = 0.0
                # Case in which separate Date and Time fields are usedd
                if self.radioDateTime.isChecked():
                    for ftime in selection :
                        if type(ftime[dateField]) == QDate or type(ftime[dateField]) == QDateTime:
                            dateTostring = ftime[dateField].toString('yyyy-MM-dd')
                            # dtemp = dt.datetime.strptime(dateTostring,dateFormat)
                            dtemp = ftime[dateField].toPyDate()
                        else:
                            dtemp = dt.datetime.strptime(ftime[dateField],dateFormat)
                            dtemp = dtemp.date()

                        if type(ftime[timeField]) == QTime :
                            timeTostring = ftime[timeField].toString('H:M:S')
                            ttemp = ftime[timeField].toPyTime()
                            # ttemp = dt.datetime.strptime(dateTostring,dateFormat)
                        else:
                            ttemp = dt.datetime.strptime(ftime[timeField],timeFormat)
                            ttemp = ttemp.toPyTime()

                        tempo.append(dt.datetime.combine(dtemp,ttemp))
            else:
                tempo = np.linspace(0,1,len(sdat))

        else:
            for f in layer.getFeatures():
                if f[index]:
                    stemp = float(f[index])
                else:
                    stemp = 0.0

                sdat.append(stemp)
			#
            if useTimeField:
                # Case in which Unique TimeStamp is used
                if self.radioTimeStamp.isChecked():
                    for ftime in layer.getFeatures():
                        if type(ftime[timestampField]) == QDateTime :
                            ttemp = ftime[timestampField].toPyDateTime()
                        else:
                            ttemp = dt.datetime.strptime(ftime[timestampField],timestampFormat)

                        tempo.append(ttemp)
                # Case in which separate Date and Time fields are usedd
                if self.radioDateTime.isChecked():
                    for ftime in layer.getFeatures() :
                        if type(ftime[dateField]) == QDate or type(ftime[dateField]) == QDateTime :
                            # dateTostring = ftime[dateField].toString('yyyy-MM-dd')
                            # dtemp = dt.datetime.strptime(dateTostring,dateFormat)
                            dtemp = ftime[dateField].toPyDate()
                        else:
                            dtemp = dt.datetime.strptime(ftime[dateField],dateFormat)
                            dtemp = dtemp.date()

                        if type(ftime[timeField]) == QTime :
                            # timeTostring = ftime[timeField].toString('H:M:S')
                            # ttemp = dt.datetime.strptime(dateTostring,dateFormat)
                            ttemp = ftime[timeField].toPyTime()
                        else:
                            ttemp = dt.datetime.strptime(ftime[timeField],timeFormat)
                            ttemp = ttemp.time()

                        tempo.append(dt.datetime.combine(dtemp,ttemp))

                # for ftime in layer.getFeatures(requestTime):
                #     if ftime[timeIndex]:
                #         if type(ftime[timeIndex]) == QDate :
                #             dateTostring = ftime[timeIndex].toString('yyyy-MM-dd')
                #             ttemp = dt.datetime.strptime(dateTostring,timeFormat)
                #         else:
                #             ttemp = dt.datetime.strptime(ftime[timeIndex],timeFormat)
                #
                #     else:
                #         ttemp = 0.0
                #     tempo.append(ttemp)
            else:
                tempo = np.linspace(0,1,len(sdat))
            #

        # Compute mean and std dev.
        # single values:
        mn1 = np.mean(sdat)
        st1 = np.std(sdat)
        # Write on text box of GUI
        self.txtMean.setText(str(mn1))
        self.txtStd.setText(str(st1))

        # array for plots
        Nsdat = len(sdat)
        mn = np.ones(Nsdat)
        mn = mn*mn1
        st = np.ones(Nsdat)
        st = st*st1
        upper = mn + st
        lower = mn - st

        self.canvas.draw()
        self.axes.set_title(self.tr("Data "))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_xlabel('Time ')
        self.axes.set_ylabel(unicode(self.cmbFields.currentText()))

        self.axes.xaxis.set_major_locator(DayLocator())
        self.axes.xaxis.set_minor_locator(HourLocator(np.arange(0, 25, 25)))
        self.axes.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d '))

        self.axes.fmt_xdata = DateFormatter('%Y-%m-%d %H:%M:%S')
        self.figure.autofmt_xdate()
        self.axes.plot(tempo, sdat, "go-", label = "Data")
        #
        self.axes.plot(tempo,upper,"-.", label = "Mean + st. dev")
        self.axes.plot(tempo,mn,"-", label = "Mean ")
        self.axes.plot(tempo,lower,"--", label = "Mean - st. dev")
        ll = self.axes.legend(loc = 2)
        ll.draggable()

        self.canvas.draw()

        # save time for further use
        return (tempo,sdat)
		#
		#
    def trendPlot(self):
        # Run again dataPlot
        (tempo,sdat) = self.dataPlot()

        #

        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        index = layer.fieldNameIndex(fieldName)
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        # sdat = []
        #
        # for f in layer.getFeatures(request):
        #     if f[index]:
        #         stemp = float(f[index])
        #     else:
        #         stemp = 0.0
        #     sdat.append(stemp)


        smoothP_text = self.txtSMparam.toPlainText()
        smoothP = float(smoothP_text)
        cycle, trend = sts.hpfilter(sdat, lamb=smoothP)

        self.axes.xaxis.set_major_locator(DayLocator())
        self.axes.xaxis.set_minor_locator(HourLocator(np.arange(0, 25, 25)))
        self.axes.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d '))

        self.axes.fmt_xdata = DateFormatter('%Y-%m-%d %H:%M:%S')
        self.figure.autofmt_xdate()

        self.axes.plot(tempo, sdat, "go-", label='Data ')
        self.axes.plot(tempo, trend,'b',label='Trend')
        self.axes.plot(tempo, cycle,'r',label='Cycle')

        ll = self.axes.legend(loc = 2)
        ll.draggable()

        self.axes.set_title(self.tr("Trend and Cycle "))
        self.axes.grid(self.chkShowGrid.isChecked())

        self.axes.set_ylabel(unicode(self.cmbFields.currentText()))

        self.axes.set_xlabel("Time")

        self.canvas.draw()

        if self.chkSave.isChecked():
            csvfilename = self.OutFilePath
            csvfile = open(csvfilename, 'w')

            #writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_ALL)

            if self.chkTimeFieldSlct.isChecked():
                header = 'date, trend, cycle \n'
                csvfile.write(header)
                tstr = [0 for i in trend]
                for i, trd  in enumerate(trend):
                    tstr[i] = tempo[i].strftime('%Y-%m-%d %H:%M:%S')
                    csvfile.write('%s,%f,%f \n'%(tstr[i],trd,cycle[i]))
            else:
                header = 'trend, cycle \n'
                csvfile.write(header)
                for i, trd  in enumerate(trend):
                    csvfile.write('%f,%f \n'%(trd,cycle[i]))

            csvfile.close()



		#
		#
    def multiPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        layer2   = utils.getVectorLayerByName(self.cmbLayers_2.currentText())
        fieldName2 = self.cmbFields_2.currentText()
        index = layer.fieldNameIndex(fieldName)
        index2 = layer.fieldNameIndex(fieldName2)
        count = layer.featureCount()
        count2 = layer2.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        request2 = QgsFeatureRequest()
        request2.setFlags(QgsFeatureRequest.NoGeometry)
        request2.setSubsetOfAttributes([index2])

        sdat = []
        dat2 = []

        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0

            sdat.append(stemp)

        for k in layer2.getFeatures(request2):
            if k[index2]:
                datemp = float(k[index2])
            else:
                datemp = 0.0

            dat2.append(datemp)


        self.canvas.draw()
        self.axes.set_title(self.tr("Multi Plot "))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_xlabel(unicode(self.cmbTime.currentText()))
        self.axes.plot(sdat, "go-", label = unicode(self.cmbFields.currentText()))
        self.axes.plot(dat2, "rs-", label = unicode(self.cmbFields_2.currentText()))
        self.axes.legend()
        self.figure.autofmt_xdate()
        self.canvas.draw()

    def acfPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        index = layer.fieldNameIndex(fieldName)
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        sdat = []

        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0

            sdat.append(stemp)


        Nsdat = len(sdat)

        # OLD: Lag is 0.1 of the time series dimension
        # OLD: lag = 0.1*Nsdat

        # Get lag value from text box in UI
        lag_text = self.txtLag.toPlainText()
        lag = float(lag_text)


        autocor = sts.acf(sdat, unbiased=False, nlags=lag, confint=None, qstat=False, fft=False, alpha=None)

        # Upper and lower limit for 5% confidence
        upr = 1.96/(np.sqrt(Nsdat))

        Nacf  = len(autocor)
        upper = np.ones(Nacf)
        upper = upr*upper
        lower = - upper

        self.axes.plot(autocor, "ro-", label='AC Function ')
        self.axes.plot(upper,"-.", label = "Upper band critical value")
        self.axes.plot(lower,"--", label = "Lower band critical value")

        self.axes.legend()

        self.axes.set_title(self.tr("Autocorrelation Function" ))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_ylabel(unicode(self.cmbFields.currentText()))

        self.figure.autofmt_xdate()
        self.canvas.draw()

		#
		#
    def ksTest(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        index = layer.fieldNameIndex(fieldName)
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        sdat = []

        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0

            sdat.append(stemp)


        Nsdat = len(sdat)



        Dtest, pvalue = scystat.kstest(sdat, 'norm')

        # Write results into UI text box
        Dtest = str(Dtest)
        pvalue = str(pvalue)
        self.txtKSstatOut.setText(Dtest)
        self.txtKSpvalue.setText(pvalue)

		#
		#
		#
    def qqPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        index = layer.fieldNameIndex(fieldName)
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        sdat = []


        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0

            sdat.append(stemp)




        Nsdat = len(sdat)


        res = scystat.probplot(sdat, plot=None)
        osm , osr = res[0]
        slope, intercept, r = res[1]
        slope, intercept, r, prob, sterrest = scystat.linregress(osm, osr)

        self.axes.plot(osm, osr, 'o')
        self.axes.plot(osm, slope*osm + intercept, 'r-')



        self.axes.legend()

        self.axes.set_title(self.tr("q-q Plot " ))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_ylabel('Ordered Values (data sample)')
        self.axes.set_xlabel('Quantiles of Normal Distribution ')

        xmin = np.amin(osm)
        xmax = np.amax(osm)
        ymin = np.amin(osr)
        ymax = np.amax(osr)
        posx = xmin + 0.70 * (xmax - xmin)
        posy = ymin + 0.01 * (ymax - ymin)
        self.axes.text(posx, posy, "R^2=%1.4f" % r)


        self.figure.autofmt_xdate()
        self.canvas.draw()

		#
		#
    def periodogramPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        index = layer.fieldNameIndex(fieldName)
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        sdat = []

        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0

            sdat.append(stemp)


        prd  = sts.periodogram(sdat)

        self.axes.hist(prd, bins=50)
        self.axes.legend()

        self.axes.set_title(self.tr("Periodogram" ))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_xlabel("Natural frequencies")
        #self.axes.set_ylabel("")

        self.figure.autofmt_xdate()
        self.canvas.draw()
		#
		#
    def scatterPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()
        layer2   = utils.getVectorLayerByName(self.cmbLayers_2.currentText())
        fieldName2 = self.cmbFields_2.currentText()
        index = layer.fieldNameIndex(fieldName)
        index2 = layer.fieldNameIndex(fieldName2)
        count = layer.featureCount()
        count2 = layer2.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([index])
        request2 = QgsFeatureRequest()
        request2.setFlags(QgsFeatureRequest.NoGeometry)
        request2.setSubsetOfAttributes([index2])

        sdat = []
        dat2 = []

        for f in layer.getFeatures(request):
            if f[index]:
                stemp = float(f[index])
            else:
                stemp = 0.0
            sdat.append(stemp)

        for k in layer2.getFeatures(request2):
            if k[index2]:
                datemp = float(k[index2])
            else:
                datemp = 0.0

            dat2.append(datemp)


        self.canvas.draw()
        self.axes.set_title(self.tr("Scatter Plot "))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_xlabel(unicode(self.cmbFields.currentText()))
        self.axes.set_ylabel(unicode(self.cmbFields_2.currentText()))
        self.axes.plot(sdat, dat2, "go")

        self.figure.autofmt_xdate()
        self.canvas.draw()


    def dataSteadyPlot(self):
        self.axes.clear()

        layer   = utils.getVectorLayerByName(self.cmbLayers.currentText())
        fieldName = self.cmbFields.currentText()

        #
        # check for (possible) selection on the selected layer
        useSelection = self.chkUseSelected.isChecked()

        if useSelection == False:
            QMessageBox.warning(self,
                                self.tr('No selection'),
                                self.tr('There is no selection in input layer!'
                                        'This might cause some error ...'))

        # check for (possible) selection of time field

        useTimeField = self.chkTimeFieldSlct.isChecked()

        if useTimeField == False:
            QMessageBox.warning(self,
                                self.tr('No Date Expression'),
                                self.tr('There is no date expression selected!'
                                        'This might cause some errors ...'))
        #
        if useTimeField:
            if self.radioTimeStamp.isChecked():
                timestampField = self.cmbTimeStamp.currentText()
                timestampFormat = self.cmbTimeStampFormat.currentText()
                index = layer.fieldNameIndex(timestampField)
            else:
                dateField = self.cmbDate.currentText()
                dateFormat = self.cmbDateFormat.currentText()
                index = layer.fieldNameIndex(dateField)
                timeField = self.cmbTime.currentText()
                timeFormat = self.cmbTimeFormat.currentText()


        #
        count = layer.featureCount()
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        #
        #
        if useTimeField:
            if self.radioDateTime.isChecked():
                dateIndex = layer.fieldNameIndex(dateField)
                # requestDate = QgsFeatureRequest()
                # requestDate.setFlags(QgsFeatureRequest.NoGeometry)
                # requestDate.setSubsetOfAttributes([timeIndex])

        dateLabel = []
        sdat = {}
        sx = {}
        fieldList = layer.fields()
        #
        if useSelection:
            selection = layer.selectedFeatures()
            count = layer.selectedFeatureCount()
            #
            k = 0
            for f in selection :
                if useTimeField:
                    dateLabelTmp = f[index]
                else:
                    dateLabelTmp.append(k)
                dateLabel.append(dateLabelTmp)
                print 'le date ---', dateLabelTmp
                sdat[dateLabelTmp] = []
                sx[dateLabelTmp] = []
                for j in range(index+1, index + 70 ):
                    try:
                        sdat[dateLabelTmp].append( float(f[j] ))
                    except:
                        sdat[dateLabelTmp].append(0)
                k += 1

        else:
            #
            k = 0
            for f in layer.getFeatures() :
                if useTimeField:
                    dateLabelTmp = f[index]
                else:
                    dateLabelTmp.append(k)
                dateLabel.append(dateLabelTmp)
                sdat[dateLabelTmp] = f[index+1:]
                k += 1


        # array for plots
        Nsdat = len(sdat.keys())

        self.canvas.draw()
        self.axes.set_title(self.tr("Data "))
        self.axes.grid(self.chkShowGrid.isChecked())
        self.axes.set_xlabel('Values at selected time ')
        self.axes.set_ylabel(unicode(self.cmbLayers.currentText()))

        for d in sdat.keys():
            print 'chiave ', d
            datum = sdat[d]
            #xtick = sx[d]
            self.axes.plot([i for i in range(len(sdat[d]))], sdat[d],  label = d.toString())
            #self.axes.plot(xtick, datum,  label = str(d))

        ll = self.axes.legend(loc = 2)
        ll.draggable()

        self.canvas.draw()

##        # save time for further use
##        return (tempo,sdat)
		#

    def keyPressEvent(self, event):
        if event.modifiers() in [Qt.ControlModifier, Qt.MetaModifier] and event.key() == Qt.Key_C:
            clipboard = QApplication.clipboard()
            clipboard.setText("\n".join(self.tableData))
        else:
            QDialog.keyPressEvent(self, event)
