1) INSTALLARE IL PLUGIN

Installare il plugin è molto semplice: è sufficiente copiare l'intera cartella 'dataexplorer' nella path dei plugin.
In Windows, essa si trova sotto la propria Home (generalmente C:\Documents and Settings\(user), dove ovviamente (user)
è il proprio nome utente) nella sottocartella:

/.qgis2/python/plugins/

Se sotto .qgis2 non trovate la sottocartella Python, significa che ancora non avete mai installato un plugin Python
nella vostta distribuzione QGis. In tal caso andate al punto 3)

2) COSA FA IL PLUGIN
Data Explorer permette di compiere analisi statistiche di base su dati
presenti in un 'campo' di una tabella attributi di un file vettoriale, caricato nella TOC di QGis.
Il layer può essere anche un semplice file *.CSV, caricato in Qgis. Pertanto, se si possiedono delle serie di dati
in un file Excel, è sufficiente salvare questo file in formato CSV, e poi importare quest'ultimo in QGis.

Con Data Explore è possibile:
- Visualizzare su un grafico i dati contenuti in un campo
- Confrontare i dati con quelli di un altro campo, utilizzando la visualizzaione multi-plot o scatter plot.
- Effettuare test statistici di Normalità (Normal q-q plot; test di Kolmogorov-Smirnov)
- Ricavare le proprietà fondamentali di una serie storica, come utile base di partenza per analisi più approfondite
(grafico di autocorrelazione, trend test (Hodrick-Prescott), periodogramma)

3) UTILIZZO DI DataExplore ASSIEME AD ALTRI PLUGIN
Data Explore nasce come sviluppo e ampliamento di un plugin esistente, STATIST, il quale fornisce
i parametri statitistici descrittivi per dati salvati come 'campo' in un layer di QGis.
STATIST è un plugin ufficiale di QGis, e pertanto può essere installato da QGis utilizzando
lo strumento accessibile dalla barra del Menu: Plugins > Gestisci e installa plugin.
Qualora STATIST sia il primo plugin installato come Utente, dopo la sua installazione la cartella

C:\Documents and Settings\(user)\.qgis2

conterrà la sottocartella:

\python\plugins

Al suo interno troverete ora la cartella del plugin 'statist'

Analogamente (se siete arrivati qui dal punto 1) ), è qui che dovrete copiare la cartella 'dataexplorer'

4) COME AVVIARE Data Explorer
Una volta installato il plugin, lo troverete sotto Plugins > DataExp
Dovreste anche trovarlo come icona aggiuntiva della scroll bar dei plugin per dati vettoriali (in genere sul lato sinistro dell'interfaccia).

5) Siete pregati di segnalare bug e/o osservazioni a:
iacopo.borsi@tea-group.com
