# -*- coding: utf-8 -*-

#******************************************************************************
#
# 
# ---------------------------------------------------------

#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


import locale

from scipy import sparse
from scipy.sparse import dia_matrix, eye as speye
from scipy.sparse.linalg import spsolve
import numpy as np

def acovf(x, unbiased=False, demean=True, fft=False):
    '''
    Autocovariance for 1D

    Parameters
    ----------
    x : array
        Time series data. Must be 1d.
    unbiased : bool
        If True, then denominators is n-k, otherwise n
    demean : bool
        If True, then subtract the mean x from each element of x
    fft : bool
        If True, use FFT convolution.  This method should be preferred
        for long time series.

    Returns
    -------
    acovf : array
        autocovariance function
    '''
    x = np.squeeze(np.asarray(x))
    if x.ndim > 1:
        raise ValueError("x must be 1d. Got %d dims." % x.ndim)
    n = len(x)

    if demean:
        xo = x - x.mean()
    else:
        xo = x
    if unbiased:
        xi = np.arange(1, n+1)
        d = np.hstack((xi, xi[:-1][::-1]))
    else:
        d = n
    if fft:
        nobs = len(xo)
        Frf = np.fft.fft(xo, n=nobs*2)
        acov = np.fft.ifft(Frf*np.conjugate(Frf))[:nobs]/d
        return acov.real
    else:
        return (np.correlate(xo, xo, 'full')/d)[n-1:]

def acf(x, unbiased=False, nlags=40, confint=None, qstat=False, fft=False,
        alpha=None):
    '''
    Autocorrelation function for 1d arrays.

    Parameters
    ----------
    x : array
       Time series data
    unbiased : bool
       If True, then denominators for autocovariance are n-k, otherwise n
    nlags: int, optional
        Number of lags to return autocorrelation for.
    confint : scalar, optional
        The use of confint is deprecated. See `alpha`.
        If a number is given, the confidence intervals for the given level are
        returned. For instance if confint=95, 95 % confidence intervals are
        returned where the standard deviation is computed according to
        Bartlett\'s formula.
    qstat : bool, optional
        If True, returns the Ljung-Box q statistic for each autocorrelation
        coefficient.  See q_stat for more information.
    fft : bool, optional
        If True, computes the ACF via FFT.
    alpha : scalar, optional
        If a number is given, the confidence intervals for the given level are
        returned. For instance if alpha=.05, 95 % confidence intervals are
        returned where the standard deviation is computed according to
        Bartlett\'s formula.

    Returns
    -------
    acf : array
        autocorrelation function
    confint : array, optional
        Confidence intervals for the ACF. Returned if confint is not None.
    qstat : array, optional
        The Ljung-Box Q-Statistic.  Returned if q_stat is True.
    pvalues : array, optional
        The p-values associated with the Q-statistics.  Returned if q_stat is
        True.

    Notes
    -----
    The acf at lag 0 (ie., 1) is returned.

    This is based np.correlate which does full convolution. For very long time
    series it is recommended to use fft convolution instead.

    If unbiased is true, the denominator for the autocovariance is adjusted
    but the autocorrelation is not an unbiased estimtor.
    '''
    nobs = len(x)
    d = nobs # changes if unbiased
    if not fft:
        avf = acovf(x, unbiased=unbiased, demean=True)
        #acf = np.take(avf/avf[0], range(1,nlags+1))
        acf = avf[:nlags+1]/avf[0]
    else:
        #JP: move to acovf
        x0 = x - x.mean()
        Frf = np.fft.fft(x0, n=nobs*2) # zero-pad for separability
        if unbiased:
            d = nobs - np.arange(nobs)
        acf = np.fft.ifft(Frf * np.conjugate(Frf))[:nobs]/d
        acf /= acf[0]
        #acf = np.take(np.real(acf), range(1,nlags+1))
        acf = np.real(acf[:nlags+1])   #keep lag 0
    if not (confint or qstat or alpha):
        return acf
    if not confint is None:
        import warnings
        warnings.warn("confint is deprecated. Please use the alpha keyword",
                      FutureWarning)
        varacf = np.ones(nlags+1)/nobs
        varacf[0] = 0
        varacf[1] = 1./nobs
        varacf[2:] *= 1 + 2*np.cumsum(acf[1:-1]**2)
        interval = stats.norm.ppf(1-(100-confint)/200.)*np.sqrt(varacf)
        confint = np.array(zip(acf-interval, acf+interval))
        if not qstat:
            return acf, confint
    if alpha is not None:
        varacf = np.ones(nlags+1)/nobs
        varacf[0] = 0
        varacf[1] = 1./nobs
        varacf[2:] *= 1 + 2*np.cumsum(acf[1:-1]**2)
        interval = stats.norm.ppf(1-alpha/2.)*np.sqrt(varacf)
        confint = np.array(zip(acf-interval, acf+interval))
        if not qstat:
            return acf, confint
    if qstat:
        qstat, pvalue = q_stat(acf[1:], nobs=nobs)  #drop lag 0
        if (confint is not None or alpha is not None):
            return acf, confint, qstat, pvalue
        else:
            return acf, qstat, pvalue

def periodogram(X):
    """
    Returns the periodogram for the natural frequency of X

    Parameters
    ----------
    X : array-like
        Array for which the periodogram is desired.

    Returns
    -------
    pgram : array
        1./len(X) * np.abs(np.fft.fft(X))**2


    References
    ----------
    Brockwell and Davis.
    """
    X = np.asarray(X)
#    if kernel == "bartlett":
#        w = 1 - np.arange(M+1.)/M   #JP removed integer division

    pergr = 1./len(X) * np.abs(np.fft.fft(X))**2
    pergr[0] = 0. # what are the implications of this?
    return pergr



def hpfilter(X, lamb=1600):
    """
    Hodrick-Prescott filter

    Parameters
    ----------
    X : array-like
        The 1d ndarray timeseries to filter of length (nobs,) or (nobs,1)
    lamb : float
        The Hodrick-Prescott smoothing parameter. A value of 1600 is
        suggested for quarterly data. Ravn and Uhlig suggest using a value
        of 6.25 (1600/4**4) for annual data and 129600 (1600*3**4) for monthly
        data.

    Returns
    -------
    cycle : array
        The estimated cycle in the data given lamb.
    trend : array
        The estimated trend in the data given lamb.

    Examples
    ---------
    >>> import statsmodels.api as sm
    >>> dta = sm.datasets.macrodata.load()
    >>> X = dta.data['realgdp']
    >>> cycle, trend = sm.tsa.filters.hpfilter(X,1600)

    Notes
    -----
    The HP filter removes a smooth trend, `T`, from the data `X`. by solving

    min sum((X[t] - T[t])**2 + lamb*((T[t+1] - T[t]) - (T[t] - T[t-1]))**2)
     T   t

    Here we implemented the HP filter as a ridge-regression rule using
    scipy.sparse. In this sense, the solution can be written as

    T = inv(I - lamb*K'K)X

    where I is a nobs x nobs identity matrix, and K is a (nobs-2) x nobs matrix
    such that

    K[i,j] = 1 if i == j or i == j + 2
    K[i,j] = -2 if i == j + 1
    K[i,j] = 0 otherwise

    References
    ----------
    Hodrick, R.J, and E. C. Prescott. 1980. "Postwar U.S. Business Cycles: An
        Empricial Investigation." `Carnegie Mellon University discussion
        paper no. 451`.
    Ravn, M.O and H. Uhlig. 2002. "Notes On Adjusted the Hodrick-Prescott
        Filter for the Frequency of Observations." `The Review of Economics and
        Statistics`, 84(2), 371-80.
    """
    #_pandas_wrapper = _maybe_get_pandas_wrapper(X)
    X = np.asarray(X, float)
    if X.ndim > 1:
        X = X.squeeze()
    nobs = len(X)
    I = speye(nobs,nobs)
    offsets = np.array([0,1,2])
    data = np.repeat([[1.],[-2.],[1.]], nobs, axis=1)
    K = dia_matrix((data, offsets), shape=(nobs-2,nobs))

    import scipy
    if (X.dtype != np.dtype('<f8') and
            int(scipy.__version__[:3].split('.')[1]) < 11):
        #scipy umfpack bug on Big Endian machines, will be fixed in 0.11
        use_umfpack = False
    else:
        use_umfpack = True

    if scipy.__version__[:3] == '0.7':
        #doesn't have use_umfpack option
        #will be broken on big-endian machines with scipy 0.7 and umfpack
        trend = spsolve(I+lamb*K.T.dot(K), X)
    else:
        trend = spsolve(I+lamb*K.T.dot(K), X, use_umfpack=use_umfpack)
    cycle = X-trend
    #~ if _pandas_wrapper is not None:
        #~ return _pandas_wrapper(cycle), _pandas_wrapper(trend)
    return cycle, trend
